let arr1 = ["travel", "hello", "eat", "ski", "lift"];
let arr2 = arr1.filter(comp);
function comp(value) {
  return value.length > 3;
}
console.log(arr2);

let people = [
  { name: "Іван", age: 25, sex: "чоловіча" },
  { name: "Володимир", age: 21, sex: "чоловіча" },
  { name: "Марія", age: 18, sex: "жіноча" },
  { name: "Дар'я", age: 20, sex: "жіноча" },
];
let male = people.filter(function (item) {
  return item.sex === "чоловіча";
});
console.log(male);
